var UserDesignationModel = require('../models/UserDesignationModel.js');

/**
 * UserDesignationController.js
 *
 * @description :: Server-side logic for managing UserDesignations.
 */
module.exports = {

    /**
     * UserDesignationController.list()
     */
    list: function (req, res) {
        UserDesignationModel.find(function (err, UserDesignations) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting UserDesignation.',
                    error: err
                });
            }
            return res.json(UserDesignations);
        });
    },

    /**
     * UserDesignationController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        UserDesignationModel.findOne({_id: id}, function (err, UserDesignation) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting UserDesignation.',
                    error: err
                });
            }
            if (!UserDesignation) {
                return res.status(404).json({
                    message: 'No such UserDesignation'
                });
            }
            return res.json(UserDesignation);
        });
    },

    /**
     * UserDesignationController.create()
     */
    create: function (req, res) {
        var UserDesignation = new UserDesignationModel({
			id : req.body.id,
			name : req.body.name,
			company_id : req.body.company_id,
			creation_user_id : req.body.creation_user_id,
			update_user_id : req.body.update_user_id,
			creation_dt : req.body.creation_dt,
			update_dt : req.body.update_dt

        });

        UserDesignation.save(function (err, UserDesignation) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating UserDesignation',
                    error: err
                });
            }
            return res.status(201).json(UserDesignation);
        });
    },

    /**
     * UserDesignationController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        UserDesignationModel.findOne({_id: id}, function (err, UserDesignation) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting UserDesignation',
                    error: err
                });
            }
            if (!UserDesignation) {
                return res.status(404).json({
                    message: 'No such UserDesignation'
                });
            }

            UserDesignation.id = req.body.id ? req.body.id : UserDesignation.id;
			UserDesignation.name = req.body.name ? req.body.name : UserDesignation.name;
			UserDesignation.company_id = req.body.company_id ? req.body.company_id : UserDesignation.company_id;
			UserDesignation.creation_user_id = req.body.creation_user_id ? req.body.creation_user_id : UserDesignation.creation_user_id;
			UserDesignation.update_user_id = req.body.update_user_id ? req.body.update_user_id : UserDesignation.update_user_id;
			UserDesignation.creation_dt = req.body.creation_dt ? req.body.creation_dt : UserDesignation.creation_dt;
			UserDesignation.update_dt = req.body.update_dt ? req.body.update_dt : UserDesignation.update_dt;
			
            UserDesignation.save(function (err, UserDesignation) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating UserDesignation.',
                        error: err
                    });
                }

                return res.json(UserDesignation);
            });
        });
    },

    /**
     * UserDesignationController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        UserDesignationModel.findByIdAndRemove(id, function (err, UserDesignation) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the UserDesignation.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
