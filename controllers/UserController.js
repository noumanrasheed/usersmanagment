var UserModel = require('../models/UserModel.js');
var DesignationModel = require('../models/DesignationModel');

/**
 * UserController.js
 *
 * @description :: Server-side logic for managing Users.
 */
module.exports = {

    /**
     * UserController.list()
     */
    list: function (req, res) {
        UserModel.find(function (err, Users) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting User.',
                    error: err
                });
            }

            DesignationModel.findById({_id: Users._id}, function(err, Designation){
                if(err) {
                    return res.status(500).json({
                        message: 'Error when getting User.',
                        error: err
                    });                }
            })

            return res.json(Users, Designation);
        });
    },

    /**
     * UserController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        UserModel.findOne({_id: id}, function (err, User) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting User.',
                    error: err
                });
            }
            if (!User) {
                return res.status(404).json({
                    message: 'No such User'
                });
            }
            return res.json(User);
        });
    },

    /**
     * UserController.create()
     */
    create: function (req, res) {
        var User = new UserModel({
			username : req.body.username,
			role_id : req.body.role_id,
			designation_id : req.body.designation_id,
			login_id : req.body.login_id,
			password : req.body.password,
			status : req.body.status,
			company_id : req.body.company_id,
			enable_login : req.body.enable_login,
			creation_user_id : req.body.creation_user_id,
			update_user_id : req.body.update_user_id,
			creation_dt : req.body.creation_dt,
			update_dt : req.body.update_dt

        });

        User.save(function (err, User) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating User',
                    error: err
                });
            }
            return res.status(201).json(User);
        });
    },

    /**
     * UserController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        UserModel.findOne({_id: id}, function (err, User) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting User',
                    error: err
                });
            }
            if (!User) {
                return res.status(404).json({
                    message: 'No such User'
                });
            }

            User.username = req.body.username ? req.body.username : User.username;
			User.role_id = req.body.role_id ? req.body.role_id : User.role_id;
			User.designation_id = req.body.designation_id ? req.body.designation_id : User.designation_id;
			User.login_id = req.body.login_id ? req.body.login_id : User.login_id;
			User.password = req.body.password ? req.body.password : User.password;
			User.status = req.body.status ? req.body.status : User.status;
			User.company_id = req.body.company_id ? req.body.company_id : User.company_id;
			User.enable_login = req.body.enable_login ? req.body.enable_login : User.enable_login;
			User.creation_user_id = req.body.creation_user_id ? req.body.creation_user_id : User.creation_user_id;
			User.update_user_id = req.body.update_user_id ? req.body.update_user_id : User.update_user_id;
			User.creation_dt = req.body.creation_dt ? req.body.creation_dt : User.creation_dt;
			User.update_dt = req.body.update_dt ? req.body.update_dt : User.update_dt;
			
            User.save(function (err, User) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating User.',
                        error: err
                    });
                }

                return res.json(User);
            });
        });
    },

    /**
     * UserController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        UserModel.findByIdAndRemove(id, function (err, User) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the User.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    },

    /**
     * UserController.login()
     */

     login: function (req, res){
        UserModel.findOne(
            {
                login_id: req.query.login_id,
                password: req.query.password
            },
            (err, user)=>{
    
                if(!err){
                    res.status(200).json({
                        message: "success",
                        data: user
                    })
                }else{
                    res.send(err);
                }
        
            })
    }
};
