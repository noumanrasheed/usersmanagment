const constant = require('../extras/constants.js')
const express = require('express');
const mongoose = require('mongoose');

const router = express.Router();
const UserModel = require('../models/UserModel');

router.post("/add_user", (req, res)=>{

    console.log(req.query);

    let newuser = new UserModel(
        {
            username: req.body.username,
            role_id : req.body.role_id,
            designation_id : req.body.designation_id,
            login_id : req.body.login_id,
            password : req.body.password,
            status: req.body.status,
            company_id: req.body.company_id,
            enable_login:req.body.enable_login,
            creation_user_id: req.body.creation_user_id,
            update_user_id: req.body.update_user_id,
            creation_dt: req.body.creation_dt,
            update_dt: req.body.update_dt

        }
    );

    newuser.save(function (err) {
        if (!err) {
            res.send('Product Created successfully')
        }else{
            res.status(200).json({
                message: "Error Creating a User",
            })
        }
    })
});

router.get("/get_all_users", (req, res)=>{
    UserModel.find( (err, users)=>{
        if(!err){
            res.send(users);
        }else{
            res.status(200).json({
                message: "Error getting Users.",
            })        }
    })

});

router.get("/get_user_by_id", (req, res)=>{

    UserModel.findById(req.query.id,(err, user)=>{

        if(!err){
            res.status(200).json({
                message: "success",
                data: user
            })
        }else{
            res.send(err);
        }

    })

});

router.get("/get_user_by_email", (req, res)=>{

    UserModel.findOne(
        {
            login_id:req.query.login_id
        },
        (err, user)=>{

        if(!err){
            res.status(200).json({
                message: "success",
                data: user
            })
        }else{
            res.send(err);
        }

    })

});

router.get("/login", (req, res)=>{
    UserModel.findOne(
        {
            login_id: req.query.login_id,
            password: req.query.password
        },
        (err, user)=>{

            if(!err){
                res.status(200).json({
                    message: "success",
                    data: user
                })
            }else{
                res.send(err);
            }
    
        })
});

router.get("/delete_user", (req, res)=>{

    UserModel.findByIdAndDelete(
        req.query.id,
        (err, user)=>{

            if(!err){
                res.status(200).json({
                    message: "success",
                    data: user
                })
            }else{
                res.send(user);                
            } 

        }
    )

});

router.put("/update_user", (req, res)=>{
    UserModel.findByIdAndUpdate(
        req.query.id,
        req.body.updateData,
        { new :true},
        (err, user)=>{
            if(!err){
                res.status(200).json({
                    message: "Success",
                    data: user
                })
            }

        }
    )
});

module.exports = router;