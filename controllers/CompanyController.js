var CompanyModel = require('../models/CompanyModel.js');

/**
 * CompanyController.js
 *
 * @description :: Server-side logic for managing Companys.
 */
module.exports = {

    /**
     * CompanyController.list()
     */
    list: function (req, res) {
        CompanyModel.find(function (err, Companys) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting Company.',
                    error: err
                });
            }
            return res.json(Companys);
        });
    },

    /**
     * CompanyController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        CompanyModel.findOne({_id: id}, function (err, Company) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting Company.',
                    error: err
                });
            }
            if (!Company) {
                return res.status(404).json({
                    message: 'No such Company'
                });
            }
            return res.json(Company);
        });
    },

    /**
     * CompanyController.create()
     */
    create: function (req, res) {
        var Company = new CompanyModel({
			id : req.body.id,
			name : req.body.name,
			tax_id : req.body.tax_id,
			creation_user_id : req.body.creation_user_id,
			update_user_id : req.body.update_user_id,
			creation_dt : req.body.creation_dt,
			update_dt : req.body.update_dt

        });

        Company.save(function (err, Company) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating Company',
                    error: err
                });
            }
            return res.status(201).json(Company);
        });
    },

    /**
     * CompanyController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        CompanyModel.findOne({_id: id}, function (err, Company) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting Company',
                    error: err
                });
            }
            if (!Company) {
                return res.status(404).json({
                    message: 'No such Company'
                });
            }

            Company.id = req.body.id ? req.body.id : Company.id;
			Company.name = req.body.name ? req.body.name : Company.name;
			Company.tax_id = req.body.tax_id ? req.body.tax_id : Company.tax_id;
			Company.creation_user_id = req.body.creation_user_id ? req.body.creation_user_id : Company.creation_user_id;
			Company.update_user_id = req.body.update_user_id ? req.body.update_user_id : Company.update_user_id;
			Company.creation_dt = req.body.creation_dt ? req.body.creation_dt : Company.creation_dt;
			Company.update_dt = req.body.update_dt ? req.body.update_dt : Company.update_dt;
			
            Company.save(function (err, Company) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating Company.',
                        error: err
                    });
                }

                return res.json(Company);
            });
        });
    },

    /**
     * CompanyController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        CompanyModel.findByIdAndRemove(id, function (err, Company) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the Company.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
