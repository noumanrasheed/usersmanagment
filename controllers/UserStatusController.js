var UserStatusModel = require('../models/UserStatusModel.js');

/**
 * UserStatusController.js
 *
 * @description :: Server-side logic for managing UserStatuss.
 */
module.exports = {

    /**
     * UserStatusController.list()
     */
    list: function (req, res) {
        UserStatusModel.find(function (err, UserStatuss) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting UserStatus.',
                    error: err
                });
            }
            return res.json(UserStatuss);
        });
    },

    /**
     * UserStatusController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        UserStatusModel.findOne({_id: id}, function (err, UserStatus) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting UserStatus.',
                    error: err
                });
            }
            if (!UserStatus) {
                return res.status(404).json({
                    message: 'No such UserStatus'
                });
            }
            return res.json(UserStatus);
        });
    },

    /**
     * UserStatusController.create()
     */
    create: function (req, res) {

        console.log(req.body);
        var UserStatus = new UserStatusModel({
			id : req.body.id,
			name : req.body.name

        });

        UserStatus.save(function (err, UserStatus) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating UserStatus',
                    error: err
                });
            }
            return res.status(201).json(UserStatus);
        });
    },

    /**
     * UserStatusController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        UserStatusModel.findOne({_id: id}, function (err, UserStatus) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting UserStatus',
                    error: err
                });
            }
            if (!UserStatus) {
                return res.status(404).json({
                    message: 'No such UserStatus'
                });
            }

            UserStatus.id = req.body.id ? req.body.id : UserStatus.id;
			UserStatus.name = req.body.name ? req.body.name : UserStatus.name;
			
            UserStatus.save(function (err, UserStatus) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating UserStatus.',
                        error: err
                    });
                }

                return res.json(UserStatus);
            });
        });
    },

    /**
     * UserStatusController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        UserStatusModel.findByIdAndRemove(id, function (err, UserStatus) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the UserStatus.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
