var UploadModel = require('../models/UploadModel.js');

/**
 * UploadController.js
 *
 * @description :: Server-side logic for managing Uploads.
 */
module.exports = {

    /**
     * UploadController.list()
     */
    list: function (req, res) {
        UploadModel.find(function (err, Uploads) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting Upload.',
                    error: err
                });
            }
            return res.json(Uploads);
        });
    },

    /**
     * UploadController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        UploadModel.findOne({_id: id}, function (err, Upload) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting Upload.',
                    error: err
                });
            }
            if (!Upload) {
                return res.status(404).json({
                    message: 'No such Upload'
                });
            }
            return res.json(Upload);
        });
    },

    /**
     * UploadController.create()
     */
    create: function (req, res) {
        var Upload = new UploadModel({
			id : req.body.id,
			link_id : req.body.link_id,
			type : req.body.type,
			creation_user_id : req.body.creation_user_id,
			update_user_id : req.body.update_user_id,
			creation_dt : req.body.creation_dt,
			update_dt : req.body.update_dt

        });

        Upload.save(function (err, Upload) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating Upload',
                    error: err
                });
            }
            return res.status(201).json(Upload);
        });
    },

    /**
     * UploadController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        UploadModel.findOne({_id: id}, function (err, Upload) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting Upload',
                    error: err
                });
            }
            if (!Upload) {
                return res.status(404).json({
                    message: 'No such Upload'
                });
            }

            Upload.id = req.body.id ? req.body.id : Upload.id;
			Upload.link_id = req.body.link_id ? req.body.link_id : Upload.link_id;
			Upload.type = req.body.type ? req.body.type : Upload.type;
			Upload.creation_user_id = req.body.creation_user_id ? req.body.creation_user_id : Upload.creation_user_id;
			Upload.update_user_id = req.body.update_user_id ? req.body.update_user_id : Upload.update_user_id;
			Upload.creation_dt = req.body.creation_dt ? req.body.creation_dt : Upload.creation_dt;
			Upload.update_dt = req.body.update_dt ? req.body.update_dt : Upload.update_dt;
			
            Upload.save(function (err, Upload) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating Upload.',
                        error: err
                    });
                }

                return res.json(Upload);
            });
        });
    },

    /**
     * UploadController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        UploadModel.findByIdAndRemove(id, function (err, Upload) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the Upload.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
