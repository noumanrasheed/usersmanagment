var DesignationModel = require('../models/DesignationModel.js');

/**
 * DesignationController.js
 *
 * @description :: Server-side logic for managing Designations.
 */
module.exports = {

    /**
     * DesignationController.list()
     */
    list: function (req, res) {
        DesignationModel.find(function (err, Designations) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting Designation.',
                    error: err
                });
            }
            return res.json(Designations);
        });
    },

    /**
     * DesignationController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        DesignationModel.findOne({_id: id}, function (err, Designation) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting Designation.',
                    error: err
                });
            }
            if (!Designation) {
                return res.status(404).json({
                    message: 'No such Designation'
                });
            }
            return res.json(Designation);
        });
    },

    show_name: function (req, res) {

        var id = req.params.id;
        DesignationModel.findOne({_id: id}, function (err, Designation) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting Designation.',
                    error: err
                });
            }
            if (!Designation) {
                return res.status(404).json({
                    message: 'No such Designation'
                });
            }
            return res.json(Designation.name);
        });

    },

    /**
     * DesignationController.create()
     */
    create: function (req, res) {
        var Designation = new DesignationModel({
            name : req.body.name,
            designation_id: req.body.designation_id,
			company_id : req.body.company_id,
			creation_user_id : req.body.creation_user_id,
			update_user_id : req.body.update_user_id,
			creation_dt : req.body.creation_dt,
			update_dt : req.body.update_dt

        });

        Designation.save(function (err, Designation) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating Designation',
                    error: err
                });
            }
            return res.status(201).json(Designation);
        });
    },

    /**
     * DesignationController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        DesignationModel.findOne({_id: id}, function (err, Designation) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting Designation',
                    error: err
                });
            }
            if (!Designation) {
                return res.status(404).json({
                    message: 'No such Designation'
                });
            }

            Designation.name = req.body.name ? req.body.name : Designation.name;
            Designation.designation_id = req.body.designation_id ? req.body.designation_id : Designation.designation_id;
			Designation.company_id = req.body.company_id ? req.body.company_id : Designation.company_id;
			Designation.creation_user_id = req.body.creation_user_id ? req.body.creation_user_id : Designation.creation_user_id;
			Designation.update_user_id = req.body.update_user_id ? req.body.update_user_id : Designation.update_user_id;
			Designation.creation_dt = req.body.creation_dt ? req.body.creation_dt : Designation.creation_dt;
			Designation.update_dt = req.body.update_dt ? req.body.update_dt : Designation.update_dt;
			
            Designation.save(function (err, Designation) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating Designation.',
                        error: err
                    });
                }

                return res.json(Designation);
            });
        });
    },

    /**
     * DesignationController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        DesignationModel.findByIdAndRemove(id, function (err, Designation) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the Designation.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
