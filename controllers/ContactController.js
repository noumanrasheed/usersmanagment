var ContactModel = require('../models/ContactModel.js');

/**
 * ContactController.js
 *
 * @description :: Server-side logic for managing Contacts.
 */
module.exports = {

    /**
     * ContactController.list()
     */
    list: function (req, res) {
        ContactModel.find(function (err, Contacts) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting Contact.',
                    error: err
                });
            }
            return res.json(Contacts);
        });
    },

    /**
     * ContactController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        ContactModel.findOne({_id: id}, function (err, Contact) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting Contact.',
                    error: err
                });
            }
            if (!Contact) {
                return res.status(404).json({
                    message: 'No such Contact'
                });
            }
            return res.json(Contact);
        });
    },

    /**
     * ContactController.create()
     */
    create: function (req, res) {
        var Contact = new ContactModel({
			id : req.body.id,
			company_id : req.body.company_id,
			name : req.body.name,
			designation_id : req.body.designation_id,
			creation_user_id : req.body.creation_user_id,
			update_user_id : req.body.update_user_id,
			creation_dt : req.body.creation_dt,
			update_dt : req.body.update_dt

        });

        Contact.save(function (err, Contact) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating Contact',
                    error: err
                });
            }
            return res.status(201).json(Contact);
        });
    },

    /**
     * ContactController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        ContactModel.findOne({_id: id}, function (err, Contact) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting Contact',
                    error: err
                });
            }
            if (!Contact) {
                return res.status(404).json({
                    message: 'No such Contact'
                });
            }

            Contact.id = req.body.id ? req.body.id : Contact.id;
			Contact.company_id = req.body.company_id ? req.body.company_id : Contact.company_id;
			Contact.name = req.body.name ? req.body.name : Contact.name;
			Contact.designation_id = req.body.designation_id ? req.body.designation_id : Contact.designation_id;
			Contact.creation_user_id = req.body.creation_user_id ? req.body.creation_user_id : Contact.creation_user_id;
			Contact.update_user_id = req.body.update_user_id ? req.body.update_user_id : Contact.update_user_id;
			Contact.creation_dt = req.body.creation_dt ? req.body.creation_dt : Contact.creation_dt;
			Contact.update_dt = req.body.update_dt ? req.body.update_dt : Contact.update_dt;
			
            Contact.save(function (err, Contact) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating Contact.',
                        error: err
                    });
                }

                return res.json(Contact);
            });
        });
    },

    /**
     * ContactController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        ContactModel.findByIdAndRemove(id, function (err, Contact) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the Contact.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
