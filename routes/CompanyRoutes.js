var express = require('express');
var router = express.Router();
var CompanyController = require('../controllers/CompanyController.js');

/*
 * GET
 */
router.get('/', CompanyController.list);

/*
 * GET
 */
router.get('/:id', CompanyController.show);

/*
 * POST
 */
router.post('/', CompanyController.create);

/*
 * PUT
 */
router.put('/:id', CompanyController.update);

/*
 * DELETE
 */
router.delete('/:id', CompanyController.remove);

module.exports = router;
