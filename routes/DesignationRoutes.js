var express = require('express');
var router = express.Router();
var DesignationController = require('../controllers/DesignationController.js');

/*
 * GET
 */
router.get('/', DesignationController.list);

/*
 * GET
 */
router.get('/:id', DesignationController.show);

router.get('/show_name/:id', DesignationController.show_name);

/*
 * POST
 */
router.post('/', DesignationController.create);

/*
 * PUT
 */
router.put('/:id', DesignationController.update);

/*
 * DELETE
 */
router.delete('/:id', DesignationController.remove);

module.exports = router;
