var express = require('express');
var router = express.Router();
var UserStatusController = require('../controllers/UserStatusController.js');

/*
 * GET
 */
router.get('/', UserStatusController.list);

/*
 * GET
 */
router.get('/:id', UserStatusController.show);

/*
 * POST
 */
router.post('/', UserStatusController.create);

/*
 * PUT
 */
router.put('/:id', UserStatusController.update);

/*
 * DELETE
 */
router.delete('/:id', UserStatusController.remove);

module.exports = router;
