var express = require('express');
var router = express.Router();
var UserDesignationController = require('../controllers/UserDesignationController.js');

/*
 * GET
 */
router.get('/', UserDesignationController.list);

/*
 * GET
 */
router.get('/:id', UserDesignationController.show);

/*
 * POST
 */
router.post('/', UserDesignationController.create);

/*
 * PUT
 */
router.put('/:id', UserDesignationController.update);

/*
 * DELETE
 */
router.delete('/:id', UserDesignationController.remove);

module.exports = router;
