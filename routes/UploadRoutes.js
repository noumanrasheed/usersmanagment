var express = require('express');
var router = express.Router();
var UploadController = require('../controllers/UploadController.js');

/*
 * GET
 */
router.get('/', UploadController.list);

/*
 * GET
 */
router.get('/:id', UploadController.show);

/*
 * POST
 */
router.post('/', UploadController.create);

/*
 * PUT
 */
router.put('/:id', UploadController.update);

/*
 * DELETE
 */
router.delete('/:id', UploadController.remove);

module.exports = router;
