const express = require('express');
const app = express();
const bodyParser =  require('body-parser');
const mongoose = require('mongoose');

const constants = require('../test/extras/constants')

const UserRoutes = require('./routes/UserRoutes')
const userStatusRoutes = require('./routes/UserStatusRoutes');
const companyRoutes = require('./routes/CompanyRoutes');
const designationRoutes = require('./routes/DesignationRoutes');

mongoose.connect("mongodb://localhost:27017/Inventory", {useNewUrlParser:true} ,(error)=>{
    if(!error){
        console.log("Success.");
    }else{
        console.log("Error connecting to Database.");
    }
});

app.use(express.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(constants.API+"/users", UserRoutes);
app.use(constants.API+"/userstatus", userStatusRoutes);
app.use(constants.API+"/company", companyRoutes);
app.use(constants.API+"/designation", designationRoutes);

app.get('/', (req, res) =>  {
    res.send('We are online');
});

app.listen(3000);