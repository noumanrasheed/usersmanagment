var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var CompanySchema = new Schema({
	'id' : Number,
	'name' : String,
	'tax_id' : String,
	'creation_user_id' : {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
   'update_user_id' : {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
	'creation_dt' : Date,
	'update_dt' : Date
});

module.exports = mongoose.model('Company', CompanySchema);
