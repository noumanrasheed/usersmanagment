var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var UserDesignationSchema = new Schema({
	'id' : Number,
	'name' : String,
	'company_id' : Number,
	'creation_user_id' : Number,
	'update_user_id' : Number,
	'creation_dt' : Date,
	'update_dt' : Date
});

module.exports = mongoose.model('UserDesignation', UserDesignationSchema);
