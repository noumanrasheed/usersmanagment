const constant = require('../extras/constants.js')

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserStatusSchema = new Schema({

    id:{
        type: Number,
        unique: true
    },
    name:{
        type: String,
        default: '',
        required: true,
    },

});

module.exports = mongoose.model(constant.UserStatusSchema, UserStatusSchema);