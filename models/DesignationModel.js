var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var DesignationSchema = new Schema({
	'name' : String,
	'designation_id' : Number,
	'company_id' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'Company'
	},
	'creation_user_id' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'User'
	},
	'update_user_id' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'User'
	},
	'creation_dt' : Date,
	'update_dt' : Date
});

module.exports = mongoose.model('Designation', DesignationSchema);
