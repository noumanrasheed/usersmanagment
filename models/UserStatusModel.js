var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var UserStatusSchema = new Schema({
	'id' : Number,
	'name' : String
});

module.exports = mongoose.model('UserStatus', UserStatusSchema);
