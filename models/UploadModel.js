var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var UploadSchema = new Schema({
	'id' : Number,
	'link_id' : Number,
	'type' : String,
	'creation_user_id' : Number,
	'update_user_id' : Number,
	'creation_dt' : Date,
	'update_dt' : Date
});

module.exports = mongoose.model('Upload', UploadSchema);
