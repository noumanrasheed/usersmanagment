const constant = require('../extras/constants.js')

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let AddressSchema = new Schema({

    id:{
        type: Number,
        unique: true
    },
    link_id:{
        type: String,
        default: '',
        required: true,
    },
    link_id:{
        type: Schema.Types.ObjectId,
        ref: constant.CompanySchema
    },
    address:{
        type: String,
        default: '',
        required: true
    },
    city:{
        type: String,
        default: '',
        required: true
    },
    country:{
        type: String,
        default: '',
        required: true
    },
    phone:{
        type: String,
        default: '',
        required: true
    },
    mobile:{
        type: String,
        default: '',
        required: true
    },
    fax:{
        type: String,
        default: '',
        required: true
    },
    email:{
        type: String,
        default: '',
        required: true
    },
    latitude:{
        type: String,
        default: '',
        required: true
    },
    longitude:{
        type: String,
        default: '',
        required: true
    },
    tax_id:{
        type: String,
        default: '',
        required: true
    },
    creation_user_id:{
        type: String,
        default: ''
    },
    update_user_id:{
        type: Date,
        default: '' 
    },
    creation_dt:{
        type: Date,
        default: Date.now
    },
    update_dt:{
        type: Date,
        default: '' 
    }
}
);

module.exports = mongoose.model(constant.AddressSchema, AddressSchema);