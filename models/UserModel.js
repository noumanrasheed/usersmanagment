var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var UserSchema = new Schema({
	'username' : String,
	'role_id' : Number,
	'designation_id' : {
        type: Schema.Types.ObjectId,
        ref: 'Designation'
    },
	'login_id' : {
        type: String,
        unique: true 
    },
	'password' : String,
	'status' : String,
    'company_id' : {
        type: Schema.Types.ObjectId,
        ref: 'Company'
    },
	'enable_login' : Number,
	'creation_user_id' : {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
   'update_user_id' : {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
   'creation_dt' : Date,
   'update_dt' : Date
});

module.exports = mongoose.model('User', UserSchema);
