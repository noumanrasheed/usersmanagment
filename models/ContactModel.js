var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var ContactSchema = new Schema({
	'id' : Number,
	'company_id' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'Company'
	},
	'name' : String,
	'designation_id' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'Designation'
	},
	'creation_user_id' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'User'
	},
	'update_user_id' : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'User'
	},
	'creation_dt' : Date,
	'update_dt' : Date
});

module.exports = mongoose.model('Contact', ContactSchema);
